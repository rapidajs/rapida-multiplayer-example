(async () => {
  const { geckos } = await import('@geckos.io/server');

  const players = {};

  const io = geckos();
  const PORT = 9208;
  io.listen(PORT);

  process.on('SIGTERM', () => {
    io.server.close();
  });

  console.log(`running on port ${PORT}`);

  io.onConnection((channel) => {
    channel.on('join-request', () => {
      players[channel.id] = {
        id: channel.id,
        x: 0,
        y: 0,
        z: 0,
      };
      channel.emit('join-response', players[channel.id]);
    });

    channel.on('player-frame', (d) => {
      const { id, up, down, left, right } = d;
      const player = players[id];
      player.up = up;
      player.down = down;
      player.left = left;
      player.right = right;
    });

    channel.onDisconnect(() => {
      delete players[channel.id];
    });
  });

  setInterval(() => {
    Object.values(players).forEach((p) => {
      const { left, right, up, down } = p;

      if (left) {
        p.x -= 0.1;
      }

      if (right) {
        p.x += 0.1;
      }

      if (up) {
        p.y += 0.1;
      }

      if (down) {
        p.y -= 0.1;
      }
    });

    io.emit('frame', {
      players,
    });
  }, 1 / 60);
})();

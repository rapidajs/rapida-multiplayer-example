# rapida-multiplayer-example

This repository contains a very simple example of how rapida could be used to create a multiplayer game. 

## Getting Started

```bash
> git clone https://gitlab.com/rapidajs/rapida-multiplayer-example.git

> cd rapida-multiplayer-example

> yarn install

> yarn serve
```

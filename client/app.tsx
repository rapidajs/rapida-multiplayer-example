import World, { Component, Entity, Space, System } from '@rapidajs/recs';
import { WebGLRenderer } from '@rapidajs/three';
import geckos, { ClientChannel } from '@geckos.io/client';
import * as React from 'react';
import { useEffect } from 'react';
import ReactDOM from 'react-dom';
import {
  AmbientLight,
  BoxGeometry,
  DirectionalLight,
  Mesh,
  MeshPhongMaterial,
  PerspectiveCamera,
  Scene,
  Vector3,
} from 'three';
import { useFirstRender } from './hooks';
import './styles.scss';

class PlayerIdentifier extends Component {
  playerId!: string;

  construct = (id: string) => {
    this.id = id;
  };
}

class PlayerMesh extends Component {
  gameScene!: Scene;

  cube!: Mesh;

  construct = ({ scene }: { scene: Scene }) => {
    this.gameScene = scene;

    const geometry = new BoxGeometry(50, 50, 50);
    const material = new MeshPhongMaterial({
      color: 'blue',
      specular: 0x111111,
      shininess: 30,
    });
    this.cube = new Mesh(geometry, material);
  };

  onInit = (): void => {
    this.gameScene.add(this.cube);
  };

  onDestroy = (): void => {
    this.gameScene.remove(this.cube!);
  };
}

class PlayerControls extends Component {
  playerMesh!: PlayerMesh;

  up!: boolean;

  down!: boolean;

  left!: boolean;

  right!: boolean;

  dirty!: boolean;

  construct = () => {
    this.up = false;
    this.down = false;
    this.left = false;
    this.right = false;
    this.dirty = false;
  };

  onInit = (): void => {
    this.playerMesh = this.entity.get(PlayerMesh);

    const W_KEY = 87;
    const S_KEY = 83;
    const A_KEY = 65;
    const D_KEY = 68;

    const onDocumentKeyDown = (event: KeyboardEvent) => {
      const keyCode = event.which;

      if (keyCode === W_KEY) {
        this.up = true;
      } else if (keyCode === S_KEY) {
        this.down = true;
      } else if (keyCode === A_KEY) {
        this.left = true;
      } else if (keyCode === D_KEY) {
        this.right = true;
      }
    };

    const onDocumentKeyUp = (event: KeyboardEvent) => {
      const keyCode = event.which;

      if (keyCode === W_KEY) {
        this.up = false;
      } else if (keyCode === S_KEY) {
        this.down = false;
      } else if (keyCode === A_KEY) {
        this.left = false;
      } else if (keyCode === D_KEY) {
        this.right = false;
      }
    };

    document.addEventListener('keyup', onDocumentKeyUp, false);
    document.addEventListener('keydown', onDocumentKeyDown, false);
  };
}

class PlayerNetworkManager extends Component {
  playerMesh!: PlayerMesh;

  playerControls!: PlayerControls;

  playerId!: string;

  io!: ClientChannel;

  construct = ({ playerId, io }: { playerId: string; io: ClientChannel }) => {
    this.playerId = playerId;
    this.io = io;
  };

  onInit = (): void => {
    this.playerMesh = this.entity.get(PlayerMesh);
    this.playerControls = this.entity.get(PlayerControls);

    this.io.on('player-position-update', (d) => {
      const data = d as { x: number; y: number; z: number };
      this.playerMesh.cube.position.set(data.x, data.y, data.z);
    });
  };

  onUpdate = (_timeElapsed: number) => {
    this.io.emit('player-frame', {
      id: this.playerId,
      up: this.playerControls.up,
      down: this.playerControls.down,
      left: this.playerControls.left,
      right: this.playerControls.right,
    });
  };
}

class NetworkManager extends System {
  playerId: string | undefined;

  io: ClientChannel;

  gameSpace: Space;

  gameScene: Scene;

  players: { [playerId: string]: Entity } = {};

  constructor({
    io,
    space,
    scene,
  }: {
    io: ClientChannel;
    space: Space;
    scene: Scene;
  }) {
    super();
    this.io = io;
    this.gameSpace = space;
    this.gameScene = scene;
  }

  onInit = (): void => {
    this.io.on('join-response', (join: unknown) => {
      const joinData = join as {
        id: string;
        position: { x: number; y: number; z: number };
      };

      this.playerId = joinData.id;

      const playerEntity = this.gameSpace.create.entity();
      playerEntity.addComponent(PlayerIdentifier, this.playerId);
      playerEntity.addComponent(PlayerMesh, { scene: this.gameScene });
      playerEntity.addComponent(PlayerControls);
      playerEntity.addComponent(PlayerNetworkManager, {
        playerId: joinData.id,
        io: this.io,
      });
      this.players[this.playerId] = playerEntity;

      // handle server frames
      this.io.on('frame', (frame: unknown) => {
        const frameData = frame as {
          players: {
            [id: string]: { id: string; x: number; y: number; z: number };
          };
        };

        Object.values(frameData.players).map(({ id, x, y, z }) => {
          // create or retrieve the entity
          let entity: Entity | undefined = this.players[id];
          if (entity === undefined) {
            entity = this.gameSpace.create.entity();
            entity.addComponent(PlayerIdentifier, id);
            entity.addComponent(PlayerMesh, { scene: this.gameScene });

            this.players[id] = entity;
          }

          // set the entities position
          entity.get(PlayerMesh).cube.position.set(x, y, z);
        });
      });
    });

    this.io.emit('join-request');
  };
}

const App = () => {
  const firstRender = useFirstRender();

  useEffect(() => {
    const world = new World();

    const renderer = new WebGLRenderer();
    document.getElementById('renderer-root')!.appendChild(renderer.domElement);

    const camera = new PerspectiveCamera();
    camera.position.set(0, 0, 500);

    const scene = new Scene();

    renderer.create.view({ scene, camera });

    const space = world.create.space();

    const directionalLight = new DirectionalLight(0xffffff, 1);
    directionalLight.position.set(300, 0, 300);
    directionalLight.lookAt(new Vector3(0, 0, 0));
    scene.add(directionalLight);

    const ambientLight = new AmbientLight(0xffffff, 0.5);
    ambientLight.position.set(0, -200, 400);
    ambientLight.lookAt(new Vector3(0, 0, 0));
    scene.add(ambientLight);

    const io = geckos({
      port: 9208,
    });

    io.onConnect((error) => {
      if (error) {
        console.error(error);
        return;
      }

      world.addSystem(
        new NetworkManager({
          io,
          space,
          scene,
        })
      );
    });

    // simple loop
    world.init();

    let lastCallTime = 0;

    const loop = (now: number) => {
      const nowSeconds = now / 1000;
      const elapsed = nowSeconds - lastCallTime;

      world.update(elapsed);
      renderer.render(elapsed);

      lastCallTime = nowSeconds;

      requestAnimationFrame(loop);
    };

    requestAnimationFrame(loop);
  }, [firstRender]);

  return <div id="renderer-root"></div>;
};

ReactDOM.render(<App />, document.getElementById('root'));
